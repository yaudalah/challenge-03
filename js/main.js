AOS.init();

const swiper = new Swiper(".swiper", {
  centeredSlides: true,
  cssMode: true,
  mousewheel: true,
  keyboard: true,
  loop: true,
  navigation: {
    nextEl: ".swiper-next",
    prevEl: ".swiper-prev",
  },
  breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 991px
    991: {
      slidesPerView: 2,
      spaceBetween: 30
    },
  }
});

$(document).on('click', '.navbar-collapse,.navbar-nav-toggler', function(e) {
  if ($(e.target).is('.navbar-collapse') || $(e.target).is('.navbar-nav-toggler')) {
      e.preventDefault();
      $('.navbar-collapse').collapse('hide');
  }
});